﻿using System;
using System.Collections.Generic;
using UPB.Practice4.Data.Models;

namespace UPB.Practice4.Data
{
    public class DbContext : IDbContext
    {
        public List<Group> GroupTable { get; set; }

        public DbContext()
        {
            GroupTable = new List<Group>()
            {
                new Group(){Id = "Group-001",Name="Stardusters",AvailableSlots=5},
                new Group(){Id = "Group-002",Name="Crusaiders",AvailableSlots=7},
                new Group(){Id = "Group-003",Name="Real Team",AvailableSlots=3}
            };
        }

        public List<Group> GetAll()
        {
            return GroupTable;
        }
        public Group AddGroup(Group group)
        {
            GroupTable.Add(group);
            return group;
        }
        public Group UpdatePerson(Group groupToUpdate)
        {
            Group foundGroup = GroupTable.Find(group=>group.Id ==groupToUpdate.Id );
            foundGroup.Id = groupToUpdate.Id;
            foundGroup.Name = groupToUpdate.Name;
            foundGroup.AvailableSlots = groupToUpdate.AvailableSlots;
            return foundGroup;
        }
        public Group DeleteGroup(Group groupToDelete)
        {
            GroupTable.RemoveAll(group => group.Id == groupToDelete.Id);
            return groupToDelete;
        }
    }
}
